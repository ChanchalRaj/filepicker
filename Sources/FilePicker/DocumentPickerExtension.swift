//
//  ImagePickerExtension.swift
//  Chanchal's Work
//
//  Created by Chanchal Raj on 24/08/2017.
//  Copyright © 2017 Chanchal's Work. All rights reserved.
//

import Foundation
import UIKit
import UniformTypeIdentifiers
import AVFoundation


// A global var to produce a unique address for the assoc object handle
private var associatedEventHandle: UInt8 = 0


public typealias FinishPickingDocumentClosure = (UIDocumentPickerViewController, URL?) -> Void
public typealias DocumentCancelClosure = (UIDocumentPickerViewController) -> Void

public typealias DocumentPickedCopmletion = (_ fileUrl: URL?) -> Void

// MARK: - Private classes
fileprivate final class FilesClosuresWrapper {
    fileprivate var didFinishPickingFile: FinishPickingDocumentClosure?
    fileprivate var didCancel: DocumentCancelClosure?
}


extension UIDocumentPickerViewController: UIDocumentPickerDelegate {
    
    open class func selectFile(sourceVC: UIViewController, supportedTypes: [UTType]?, completion: @escaping DocumentPickedCopmletion) {
        
        var documentPicker: UIDocumentPickerViewController!
        documentPicker = UIDocumentPickerViewController(forOpeningContentTypes: supportedTypes ?? [.pdf])
        documentPicker.allowsMultipleSelection = false
        documentPicker.modalPresentationStyle = .formSheet
        
        documentPicker.didFinishPickingFile = { picker, documentURL in
            completion(documentURL)
        }
        
        documentPicker.didCancel = { picker in
            picker.dismiss(animated: true, completion: nil)
        }
        
        sourceVC.present(documentPicker, animated: true)
    }
    
    /// The closure that fires after the receiver finished picking up an image.
    public var didFinishPickingFile: FinishPickingDocumentClosure? {
        set { self.closuresWrapper.didFinishPickingFile = newValue }
        get { return self.closuresWrapper.didFinishPickingFile }
    }
    
    /// The closure that fires after the user cancels out of picker.
    public var didCancel: DocumentCancelClosure? {
        set { self.closuresWrapper.didCancel = newValue }
        get { return self.closuresWrapper.didCancel }
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        self.closuresWrapper.didFinishPickingFile?(controller,urls.first)
    }
    
    public func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        self.closuresWrapper.didCancel?(controller)
    }
    
    private var closuresWrapper: FilesClosuresWrapper {
        get {
            if let wrapper = objc_getAssociatedObject(self, &associatedEventHandle) as? FilesClosuresWrapper {
                return wrapper
            }
            
            let closuresWrapper = FilesClosuresWrapper()
            self.closuresWrapper = closuresWrapper
            return closuresWrapper
        }
        
        set {
            self.delegate = self
            objc_setAssociatedObject(self, &associatedEventHandle, newValue,
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
}

extension UIDocumentPickerViewController {
    
    class func selectFileFromFiles(viewController: UIViewController, fileTypesSupported: [UTType]?, completion: @escaping DocumentPickedCopmletion) {
        UIDocumentPickerViewController.selectFile(sourceVC: viewController, supportedTypes: fileTypesSupported) { documentURL in
            completion(documentURL)
        }
    }
}


