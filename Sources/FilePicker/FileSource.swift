//
//  File 2.swift
//  
//
//  Created by ChanLi on 27/02/2021.
//

import Foundation
import UIKit

public enum FileSource {
    case gallery(UIImage)
    case camera(UIImage)
    case files(URL)
}
