//
//  File.swift
//  
//
//  Created by ChanLi on 01/03/2021.
//

//import Foundation
//import UIKit
//extension UIAlertController{
//    class func showSettingsAlert(viewController:UIViewController,title:String,message:String){
//            DispatchQueue.main.async {
//                let cancel = UIAlertAction.init(title: "Cancel", style: .default, handler: nil)
//                let settings = UIAlertAction.init(title: "Settings", style: .default) { (action) in
//                    if let url = URL(string:UIApplication.openSettingsURLString){
//                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
//                    }
//                }
//                let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
//                alert.addAction(cancel)
//                alert.addAction(settings)
//                alert.preferredAction = settings
//                viewController.present(alert, animated: true, completion: nil)
//            }
//        }
//}
import Foundation
import UIKit
extension UIAlertAction{
    enum Types {
        case cancel(style:UIAlertAction.Style = .default,handler:((UIAlertAction) -> Void)? = nil)
        case settings(style:UIAlertAction.Style = .cancel,handler:((UIAlertAction) -> Void)? = nil)
        case ok(style:UIAlertAction.Style = .default,handler:((UIAlertAction) -> Void)? = nil)
        case other(title:String,style:UIAlertAction.Style = .default,handler:((UIAlertAction) -> Void)? = nil)
        func value()->UIAlertAction{
            switch self{
            case .other(let title, let style, let handler):
                return UIAlertAction.init(title: title, style:style, handler: handler)
            case .ok(let style,let handler):
                return UIAlertAction.init(title: "general_ok".localized, style:style, handler: handler)
            case .cancel(let style,let handler):
                return UIAlertAction.init(title: "general_cancel".localized, style:style, handler: handler)
            case .settings(let style,let handler):
                return UIAlertAction.init(title: "general_settings".localized, style:style,handler: handler)
            }
        }
    }
}
extension UIAlertController{
    class func showSettingsAlert(viewController:UIViewController,title:String,message:String){
        DispatchQueue.main.async {
            let cancel = UIAlertAction.Types.cancel(style: .default, handler: nil).value()
            let settings = UIAlertAction.Types.settings(style: .default) { (action) in
                if let url = URL(string:UIApplication.openSettingsURLString){
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }.value()
            UIAlertController.showAlert(in: viewController, title: title, message: message, preferredActionIndex: 1,actions: cancel,settings)
        }
    }
    /**
     Shows UIAlertController with given parameters
     - Parameter viewController: The UIViewController which the UIAlertController will be shown on
     - Parameter title: The title of the UIAlertController
     - Parameter message: The message of the UIAlertController
     - Parameter style: The style of the UIAlertController i.e. either alert or actionsheet. **alert** style is set as default.
     - Parameter actions: The list of actions to be added on UIAlertController
     */
    class func showAlert(in viewController:UIViewController,title:String,message:String,style:UIAlertController.Style = .alert,preferredActionIndex:Int? = nil,actions:UIAlertAction...){
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: style)
        for action in actions{
            alert.addAction(action)
        }
        if let preferredActionIndex = preferredActionIndex, preferredActionIndex < actions.count{
            let preferredAction = actions[preferredActionIndex]
            alert.preferredAction = preferredAction
        }
        viewController.present(alert, animated: true, completion: nil)
    }
}
