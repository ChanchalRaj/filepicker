//
//  File.swift
//  
//
//  Created by ChanLi on 01/03/2021.
//

import Foundation
extension String {
    var localized:String{
        return NSLocalizedString(self, tableName: nil, bundle: .module, value: "", comment: "")
    }
    func localized(arguments: CVarArg...) -> String{
        let string = NSLocalizedString(self, tableName: nil, bundle: .module, value: "", comment: "")
        let localizedString = String.init(format: string, arguments: arguments)
        return localizedString
    }
}
