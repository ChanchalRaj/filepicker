//
//  ImagePickerExtension.swift
//  Chanchal's Work
//
//  Created by Chanchal Raj on 24/08/2017.
//  Copyright © 2017 Chanchal's Work. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

public typealias LKFinishPickingMediaClosure = (UIImagePickerController, [UIImagePickerController.InfoKey: Any]) -> Void
public typealias LKCancelClosure = (UIImagePickerController) -> Void

// A global var to produce a unique address for the assoc object handle
private var associatedEventHandle: UInt8 = 0

/// UIImagePickerController with closure callback(s).
///
/// Example:
///
/// ```swift
/// let picker = UIImagePickerController()
/// picker.didCancel = { picker in
///     print("DID CANCEL! \(picker)")
/// }
/// picker.didFinishPickingMedia = { picker, media in
///     print("Media: \(media[UIImagePickerControllerEditedImage])")
/// }
/// self.presentViewController(picker, animated: true, completion: nil)
/// ```
extension UIImagePickerController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    public enum CameraError:Error {
        case sourceTypeNotSupported
        case accessDenied
    }
    open class func takePicture(viewController:UIViewController,sourceType:SourceType,cameraDevice:CameraDevice = .rear,didFinishPickingMedia:@escaping(Result<UIImage,CameraError>)->Void,didCancel:@escaping()->Void){
        
        func callImagePicker(){
            if UIImagePickerController.isSourceTypeAvailable(sourceType){
                let picker = UIImagePickerController.init()
                picker.sourceType = sourceType
                if sourceType == .camera{
                    picker.cameraDevice = cameraDevice
                }
                picker.didFinishPickingMedia = {picker, media in
                    guard let image = media[.originalImage] as? UIImage else {return}
                    picker.dismiss(animated: true, completion: {
                        didFinishPickingMedia(.success(image))
                    })
                }
                picker.didCancel = { picker in
                    picker.dismiss(animated: true, completion: nil)
                }
                viewController.present(picker, animated: true, completion: nil)
            }else{
                didFinishPickingMedia(.failure(.sourceTypeNotSupported))
            }
            
        }
        
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
        switch cameraAuthorizationStatus{
            case .authorized,.notDetermined:
                callImagePicker()
            case .denied,.restricted:
                didFinishPickingMedia(.failure(.accessDenied))
            @unknown default:
                callImagePicker()
        }
        
        
    }
    private class func isAuthorized()->Bool?{
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            //already authorized
            return true
        } else if AVCaptureDevice.authorizationStatus(for: .video) == .denied{
            return false
        }else{
            return nil
        }
    }
    private var closuresWrapper: ClosuresWrapper {
        get {
            if let wrapper = objc_getAssociatedObject(self, &associatedEventHandle) as? ClosuresWrapper {
                return wrapper
            }
            
            let closuresWrapper = ClosuresWrapper()
            self.closuresWrapper = closuresWrapper
            return closuresWrapper
        }
        
        set {
            self.delegate = self
            objc_setAssociatedObject(self, &associatedEventHandle, newValue,
                                     objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    /// The closure that fires after the receiver finished picking up an image.
    public var didFinishPickingMedia: LKFinishPickingMediaClosure? {
        set { self.closuresWrapper.didFinishPickingMedia = newValue }
        get { return self.closuresWrapper.didFinishPickingMedia }
    }
    
    /// The closure that fires after the user cancels out of picker.
    public var didCancel: LKCancelClosure? {
        set { self.closuresWrapper.didCancel = newValue }
        get { return self.closuresWrapper.didCancel }
    }
    
    // MARK: UIImagePickerControllerDelegate implementation
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.closuresWrapper.didFinishPickingMedia?(picker,info)
    }
    open func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.closuresWrapper.didCancel?(picker)
    }

}

// MARK: - Private classes
fileprivate final class ClosuresWrapper {
    fileprivate var didFinishPickingMedia: LKFinishPickingMediaClosure?
    fileprivate var didCancel: LKCancelClosure?
}

extension UIImagePickerController{
    /**
     Takes picture from camera, if user has not allowed taking picture from camera,
     it shows a popup to let user go to settings to change the camera permission
     */
    class func takePictureFromCamera(viewController:UIViewController,completion:@escaping(UIImage?)->Void){
        UIImagePickerController.takePicture(viewController: viewController, sourceType: .camera, cameraDevice: .front, didFinishPickingMedia: {[weak viewController] (result:Result<UIImage, UIImagePickerController.CameraError>) in
            switch result{
            case .success(let image):
                completion(image)
            case .failure(let error):
                switch error{
                case .accessDenied:
                    //Ask user to go to settings
                    guard let viewController = viewController else{
                        completion(nil)
                        return
                    }
                    
                    UIAlertController.showSettingsAlert(viewController: viewController, title: "system_ios_msg_camera_access_denied_title".localized, message: "system_ios_msg_camera_access_denied_description".localized)
                    completion(nil)
                case .sourceTypeNotSupported:
                    if let viewController = viewController{
                        let title = "system_ios_msg_camera_access_denied_title".localized
                        let message = "system_ios_msg_camera_access_denied_description".localized
                        let ok = UIAlertAction.Types.ok(style: .default, handler: nil).value()
                        UIAlertController.showAlert(in: viewController, title: title, message: message, preferredActionIndex: 1,actions: ok)
                    }
                    completion(nil)
                }
                
            }
            
        }) {
            //cancelled
        }
    }
    class func takePictureFromGallery(viewController:UIViewController,completion:@escaping(UIImage?)->Void){
        UIImagePickerController.takePicture(viewController: viewController, sourceType: .photoLibrary, didFinishPickingMedia: {[weak viewController] (result:Result<UIImage, UIImagePickerController.CameraError>) in
            switch result{
            case .success(let image):
                completion(image)
            case .failure(let error):
                switch error{
                case .accessDenied:
                    //Ask user to go to settings
                    guard let viewController = viewController else{
                        completion(nil)
                        return
                    }
                    
                    UIAlertController.showSettingsAlert(viewController: viewController, title: "system_ios_msg_gallery_access_denied_title".localized, message: "system_ios_msg_camera_access_denied_description".localized)
                    completion(nil)
                case .sourceTypeNotSupported:
                    if let viewController = viewController{
                        let title = "system_ios_msg_gallery_access_denied_title".localized
                        let message = "system_ios_msg_gallery_access_denied_description".localized
                        let ok = UIAlertAction.Types.ok(style: .default, handler: nil).value()
                        UIAlertController.showAlert(in: viewController, title: title, message: message, preferredActionIndex: 1,actions: ok)
                    }
                    completion(nil)
                }
                
            }
            
        }) {
            //cancelled
        }
    }
}
