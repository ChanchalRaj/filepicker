import UIKit
import Foundation
import UniformTypeIdentifiers

public struct CGFilePicker {
    var text = "Hello, World!"
    public static func pickFile(sourceType: FileSource, fileSupportTypes: [UTType]? = [], sourceVC:UIViewController, completion:@escaping(Result<File,Error>)->Void){
        switch sourceType {
        case .camera:
            imagePicker(sourceVC: sourceVC, sourceType: .camera) { (image) in
                if let image = image {
                    let file = File(source: .camera(image))
                    completion(.success(file))
                }else{
                    //completion(.failure(<#T##Error#>))
                }
            }
        case .gallery:
            imagePicker(sourceVC: sourceVC, sourceType: .photoLibrary) { (image) in
                if let image = image {
                    let file = File(source: .gallery(image))
                    completion(.success(file))
                }else{
                    //completion(.failure(<#T##Error#>))
                }
            }
        case .files:
            chooseFile(sourceVC: sourceVC, fileTypesSupported: fileSupportTypes) { documentURL in
                if let _url = documentURL {
                    let file = File(source: .files(_url))
                    completion(.success(file))
                } else {
                    //completion(.failure(Error()))
                }
                
            }
            break
        }
    }
    
    public static func chooseFile(sourceVC: UIViewController, fileTypesSupported: [UTType]?, completion: @escaping DocumentPickedCopmletion) {
        UIDocumentPickerViewController.selectFile(sourceVC: sourceVC, supportedTypes: fileTypesSupported) { fileUrl in
            completion(fileUrl)
        }
    }
    
    public static func imagePicker(sourceVC:UIViewController, sourceType: UIImagePickerController.SourceType,completion:@escaping(UIImage?)->Void){
        if sourceType == .camera{
            UIImagePickerController.takePictureFromCamera(viewController: sourceVC, completion: completion)
        }else if sourceType == .photoLibrary{
            UIImagePickerController.takePictureFromGallery(viewController: sourceVC, completion: completion)
        }
        
    }
}
